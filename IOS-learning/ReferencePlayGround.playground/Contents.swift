//: Playground - noun: a place where people can play

import UIKit

public func test(_ name: String , _ event:(()->Void)){
    print("\n\nTest: " + name)
    event()
}

test("Flujo normal"){
    
    class Person {
        let name: String
        init(name: String) {
            self.name = name
            print("\(name) is being initialized")
        }
        deinit {
            print("\(name) is being deinitialized")
        }
    }
    
    print("------------ init test -----------")
    var reference1: Person?
    var reference2: Person?
    var reference3: Person?
    
    reference1 = Person(name: "John Appleseed")
    reference2 = reference1
    reference3 = reference1
    
    print("------------ end test ------------")
}

test("Flujo Strong"){
    
    class Person {
        let name: String
        var car: Car?
        init(name: String) {
            self.name = name
            print("\(name) is being initialized")
        }
        deinit {
            print("\(name) is being deinitialized")
        }
    }
    
    class Car{
        let serial: String
        var user: Person?
        
        init(serial: String){
            self.serial = serial
            print("\(serial) is being initialized")
        }
        
        deinit {
            print("\(serial) is being deinitialized")
        }
    }
    print("------------ init test -----------")
    var john: Person?
    var unit4A: Car?
    
    john = Person(name: "John Appleseed")
    unit4A = Car(serial: "Sedan")
    
    john!.car = unit4A
    unit4A!.user = john
    
    print("------------ end test ------------")
}



test("Flujo weak"){
    
    class Person {
        let name: String
        var car: Car?
        init(name: String) {
            self.name = name
            print("\(name) is being initialized")
        }
        deinit {
            print("\(name) is being deinitialized")
        }
    }
    
    class Car{
        let serial: String
        weak var user: Person?
        
        init(serial: String){
            self.serial = serial
            print("\(serial) is being initialized")
        }
        
        deinit {
            print("\(serial) is being deinitialized")
        }
    }
    print("------------ init test -----------")
    var john: Person?
    var unit4A: Car?
    
    john = Person(name: "John Appleseed")
    unit4A = Car(serial: "Sedan")
    
    john!.car = unit4A
    unit4A!.user = john
    
    print("------------ end test ------------")
}

test("Flujo unowed"){
    
    class Customer {
        let name: String
        var card: CreditCard?
        init(name: String) {
            self.name = name
            print("\(name) is being initialized")
        }
        deinit { print("\(name) is being deinitialized") }
    }
    
    class CreditCard {
        let number: UInt64
        unowned let customer: Customer
        init(number: UInt64, customer: Customer) {
            self.number = number
            self.customer = customer
            print("\(number) is being initialized")
        }
        deinit { print("Card #\(number) is being deinitialized") }
    }
    print("------------ init test -----------")
    var john: Customer?
    john = Customer(name: "John Appleseed")
    john!.card = CreditCard(number: 1234_5678_9012_3456, customer: john!)
    
    print("------------ end test ------------")
}

test("Flujo unowed"){
    
    class Customer {
        let name: String
        var card: CreditCard?
        init(name: String) {
            self.name = name
            print("\(name) is being initialized")
        }
        deinit { print("\(name) is being deinitialized") }
    }
    
    class CreditCard {
        let number: UInt64
        unowned let customer: Customer
        init(number: UInt64, customer: Customer) {
            self.number = number
            self.customer = customer
            print("\(number) is being initialized")
        }
        deinit { print("Card #\(number) is being deinitialized") }
    }
    print("------------ init test -----------")
    var john: Customer?
    john = Customer(name: "John Appleseed")
    john!.card = CreditCard(number: 1234_5678_9012_3456, customer: john!)
    
    print("------------ end test ------------")
}
