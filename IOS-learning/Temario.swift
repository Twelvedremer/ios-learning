//
//  Temario.swift
//  IOS-learning
//
//  Created by Momentum Lab 1 on 11/3/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation

enum Temario: String {
    case audio = "Audio"
    case urlSession = "URLSession"
    case coreData = "Core Data"
    
    
    var url: String? {
        return temarioViews[self]?.1
    }
    
    var storyboard: String? {
        return temarioViews[self]?.0
    }
}

// Orden de los temas
var temarioList:[Temario] = [.audio,.urlSession,.coreData]

// Orden de los temas
var temarioViews:[Temario: (String,String)] = [.audio:("Main", "AudioViewController"),
                                               .urlSession:("Main","SessionViewController"),
                                    .coreData:("Main","CoreViewController")]

