//
//  ViewController.swift
//  IOS-learning
//
//  Created by Momentum Lab 1 on 11/2/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        redirectView(temario: temarioList[indexPath.row])
    }
    
}

extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return temarioList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "idCell", for: indexPath)
        cell.textLabel?.text = temarioList[indexPath.row].rawValue
        return cell
    }
}

extension ViewController{
    func redirectView(temario: Temario){
        guard let url = temario.url, let storyboardName = temario.storyboard else { return }
        let vc = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: url)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

